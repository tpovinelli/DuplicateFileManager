package com.tom.dfm;

import java.util.HashMap;

/**
 * @author Thomas Povinelli
 * Created 12/12/17
 * In DuplicateFiles
 */
public class AppProperties {
    final HashMap<String, String> ButtonNames = new HashMap<>();
    final HashMap<String, String> LabelText = new HashMap<>();
    final HashMap<String, String> CheckBoxLabels = new HashMap<>();
    final HashMap<String, String> ColumnHeaderText = new HashMap<>();
    final HashMap<String, String> HashColumnNames = new HashMap<>();
    final HashMap<String, String> Messages = new HashMap<>();
    final String WindowTitle, DeleteMessageTitle, DeleteMessageHeaderText, DeleteMessageContentText;

  public AppProperties(String windowTitle, String deleteMessageTitle,
                       String deleteMessageHeaderText,
                       String deleteMessageContentText)
    {
        WindowTitle = windowTitle;
        DeleteMessageTitle = deleteMessageTitle;
        DeleteMessageHeaderText = deleteMessageHeaderText;
        DeleteMessageContentText = deleteMessageContentText;
    }

    enum Messages {
        NotImplementedMessage,
        DisclaimerMessage,

    }

    enum ButtonNames {
        ExportButtonText,
        DeleteAllButtonText,
        ShowOnDiskButtonText,
        OpenFileButtonText,
        DeleteFileButtonText,
        AddLocationButton,
        StopButtonText,
        ClearSelectionButtonText,
        ClearAllListedButtonText,
        DeleteEveryCopyText
    }

    enum CheckBoxLabels {
        recursiveBoxLabel
    }

    enum LabelText {
        progressLabelText,
        methodLabelText,
        hashingLabelText,
        doneLabelText,
        locationLabelText
    }

    enum ColumnHeaderText {
        NameColumn,
        PathColumn

    }

    enum HashColumnNames {
        md5,
        sha1,
        sha256,
        sha512
    }
}

