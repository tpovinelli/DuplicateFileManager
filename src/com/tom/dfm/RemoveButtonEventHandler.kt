package com.tom.dfm

import javafx.event.Event
import javafx.event.EventHandler
import javafx.scene.control.Label
import tom.utils.file.containsFile
import java.io.File
import java.io.IOException
import java.util.*

/**
 * @author Thomas Povinelli
 * Created 2017-Dec-28
 * In DuplicateFileManager
 */
class RemoveButtonEventHandler<T : Event>(private val gui: AppGUI, private val label: Label,
                                          private val directory: File) : EventHandler<T> {

    override fun handle(event: T) {
        gui.locationDirLabels.remove(label)

        val filesToRemove = ArrayList<File>()

        val parent = gui.directories[gui.directories.indexOf(directory)]
        for ((key, value) in gui.files) {
            for (file in value) {
                try {
                    if (parent containsFile file.file) {
                        filesToRemove.add(key)
                    }
                } catch (e: IOException) {
                    throw RuntimeException(e)
                }

            }
        }

        for (file in filesToRemove) {
            gui.files.remove(file)
        }
        gui.files.remove(parent)

        gui.directories.remove(directory)
        gui.removeButtons.remove(event.source)

        gui.controller.updateLabelsAction()
        gui.controller.updateTableAction()

    }
}
