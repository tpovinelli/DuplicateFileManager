package com.tom.dfm

/**
 * @author Thomas Povinelli
 * Created 12/15/17
 * In DuplicateFiles
 *
 * This class is used to contain the preferences of the user. Currently this consists
 * of the size of the window and the hash method used. These are stored as a JSON
 * on every window close request and read in and loaded every time the program
 * runs. Gson is used to build and serialize this class into a string for
 * loading and saving preferences. See [dfm.Driver] for the place in which
 * these preferences are saved and loaded
 */
class Preferences() {
    var windowHeight: Double = 0.0
    var windowWidth: Double = 0.0
    var hashMethod: String = "MD5"


    constructor(windowHeight: Double, windowWidth: Double,
                hashMethod: String): this() {

        this.windowHeight = windowHeight
        this.windowWidth = windowWidth
        this.hashMethod = hashMethod
    }

    override fun toString(): String {
        return "Preferences(windowHeight=$windowHeight, " +
               "windowWidth=$windowWidth, " +
               "hashMethod='$hashMethod')"
    }

    fun toJSONString(): String {
        return "{\"windowHeight\": $windowHeight, \"windowWidth\": " +
               "$windowWidth, \"hashMethod\": \"$hashMethod\"}"
    }


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Preferences) return false

        if (windowHeight != other.windowHeight) return false
        if (windowWidth != other.windowWidth) return false
        if (hashMethod != other.hashMethod) return false

        return true
    }

    override fun hashCode(): Int {
        var result = windowHeight.hashCode()
        result = 31 * result + windowWidth.hashCode()
        result = 31 * result + hashMethod.hashCode()
        return result
    }


}
