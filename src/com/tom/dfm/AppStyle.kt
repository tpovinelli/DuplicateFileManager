package com.tom.dfm

import javafx.geometry.Insets
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

/**
 * @author Thomas Povinelli
 * Created 5/12/17
 * In DuplicateFiles
 */


object AppStyle {

    @JvmOverloads
    @JvmStatic
    fun initStyle(node: GridPane, padding: Insets = Insets(5.0),
                  hgap: Double = 5.0, vgap: Double = 5.0) {
        node.padding = padding
        node.hgap = hgap
        node.vgap = vgap
    }

    @JvmOverloads
    @JvmStatic
    fun initStyle(node: HBox, padding: Insets = Insets(5.0),
                  spacing: Double = 5.0) {
        node.padding = padding
        node.spacing = spacing
    }

    @JvmOverloads
    @JvmStatic
    fun initStyle(node: VBox, padding: Insets = Insets(5.0),
                  spacing: Double = 5.0) {
        node.padding = padding
        node.spacing = spacing
    }


}
