package com.tom.dfm; /**
 * @author Thomas Povinelli
 * Created 5/12/17
 * In DuplicateFiles
 */

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class HashFile {

  @NotNull
  private SimpleObjectProperty<File> file;
  @NotNull
  private SimpleStringProperty hash;
  @NotNull
  private SimpleStringProperty name;
  // this is needed for the deleteAll action so that files are not removed
  // from lists early but also not deleted twice
  @NotNull
  private SimpleBooleanProperty deleted;

  public HashFile(@NotNull File file, @NotNull String hash) {
    this.file = new SimpleObjectProperty<>();
    this.hash = new SimpleStringProperty();
    this.name = new SimpleStringProperty();
    this.deleted = new SimpleBooleanProperty(false);
    this.file.set(file);
    this.hash.set(hash);
    this.name.set(file.getName());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    HashFile hashFile = (HashFile) o;

    if (!file.equals(hashFile.file)) return false;
    if (!hash.equals(hashFile.hash)) return false;
    if (!name.equals(hashFile.name)) return false;
    return deleted.equals(hashFile.deleted);

  }

  @Override
  public int hashCode() {
    int result = file.hashCode();
    result = 31 * result + hash.hashCode();
    result = 31 * result + name.hashCode();
    result = 31 * result + deleted.hashCode();
    return result;
  }

  @NotNull
  public boolean isDeleted() {
    return deleted.get();
  }

  public void setDeleted(@NotNull boolean deleted) {
    this.deleted.set(deleted);
  }

  @NotNull
  public SimpleBooleanProperty deletedProperty() {
    return deleted;
  }

  @NotNull
  public final File getFile() {
    return this.file.get();
  }

  public final void setFile(@NotNull File file) {
    this.file.set(file);
  }

  @NotNull
  public final String getHash() {
    return this.hash.get();
  }

  public final void setHash(@NotNull String hash) {
    this.hash.set(hash);
  }

  @NotNull
  public final String getName() {
    return this.name.get();
  }

  public final void setName(@NotNull String name) {
    this.name.set(name);
  }

  @NotNull
  public String toString() {
    return "HashFile[" + this.name + "] -> " + this.hash;
  }

  public final void set(@NotNull File file, @NotNull String hash) {
    this.file.set(file);
    this.name.set(file.getName());
    this.hash.set(hash);
  }

  @NotNull
  public final SimpleObjectProperty fileProperty() {
    return this.file;
  }

  @NotNull
  public final SimpleStringProperty hashProperty() {
    return this.hash;
  }

  @NotNull
  public final SimpleStringProperty nameProperty() {
    return this.name;
  }
}
