package com.tom.dfm

//import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.ScrollPane
import javafx.stage.Screen
import javafx.stage.Stage
import tom.utils.javafx.defaultWindowedExceptionHandler
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.io.Reader


/**
 * @author Thomas Povinelli
 * Created 5/11/17
 * In DuplicateFiles
 */

inline fun <reified T : Any> Gson.fromJson(reader: Reader): T? = fromJson<T>(reader, T::class.java)

class Driver: Application() {

    @Throws(Exception::class)
    override fun start(primaryStage: Stage) {


        val gui = AppGUI(primaryStage)
        val p = gui.root
        val controller = gui.controller

        val gson = Gson()

        val preferences: Preferences? = try {
            gson.fromJson(FileReader(gui.appPreferencesPath))
        } catch (e: IOException) {
            System.err.println("Could not open or read preferences file")
            System.err.println(e.message)
            null
        } catch (i: JsonSyntaxException) {
            System.err.println("Could not read syntax of preferences file")
            System.err.println(i.message)
            null
        }

        controller.selectHashMethod(preferences?.hashMethod ?: "MD5")

        val outer = ScrollPane(p)

        primaryStage.minWidth = 800.0
        primaryStage.minHeight = 600.0

        primaryStage.width = preferences?.windowWidth ?: Screen.getPrimary().bounds.width - 5
        primaryStage.height = preferences?.windowHeight ?: Screen.getPrimary().bounds.height - 50

        outer.maxHeightProperty().bind(primaryStage.heightProperty().subtract(25))
        outer.minHeightProperty().bind(primaryStage.heightProperty().subtract(25))
        outer.maxWidthProperty().bind(primaryStage.widthProperty().subtract(25))
        outer.minWidthProperty().bind(primaryStage.widthProperty().subtract(25))

        // we want to avoid scrollbars when the window shrinks to its minimum so
        // we allow a 10 pixel buffer
        p.minWidthProperty().bind(outer.widthProperty().subtract(15))
        p.minHeightProperty().bind(outer.heightProperty().subtract(15))
        p.maxWidthProperty().bind(outer.widthProperty().subtract(15))
        p.maxHeightProperty().bind(outer.heightProperty().subtract(15))

        primaryStage.setOnCloseRequest {
            val newPreferences = Preferences()
            newPreferences.windowHeight = primaryStage.height
            newPreferences.windowWidth = primaryStage.width
            newPreferences.hashMethod = gui.methodSelector.selectionModel.selectedItem.toString()

            val prefString = gson.toJson(newPreferences, Preferences::class.java)

            var fw: FileWriter? = null
            try {
                fw = FileWriter(gui.appPreferencesPath)
                fw.write(prefString)
            } catch (e: IOException) {
                System.err.println("Could not save preferences file")
            } finally {
                fw?.close()
            }
        }

        primaryStage.scene = Scene(outer)
        primaryStage.title = gui.appProperties.WindowTitle
        Thread.setDefaultUncaughtExceptionHandler(::defaultWindowedExceptionHandler)
        primaryStage.show()
    }

    companion object {

        @JvmStatic
        var debug: Boolean = false

        @JvmStatic
        fun main(args: Array<String>) {
            if (args.isNotEmpty() && args[0].toUpperCase() == "-D") {
                debug = true
            }
            Application.launch(Driver::class.java)
        }
    }
}
