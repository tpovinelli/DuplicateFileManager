package com.tom.dfm

import com.google.gson.Gson
import javafx.application.Platform
import javafx.beans.NamedArg
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.FXCollections.observableArrayList
import javafx.collections.ObservableList
import javafx.collections.ObservableMap
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.input.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.stage.Stage
import org.apache.commons.codec.digest.DigestUtils
import tom.utils.javafx.isCopyEvent
import tom.utils.javafx.isCutEvent
import java.io.File
import java.io.FileReader
import java.io.InputStream


/**
 * @author Thomas Povinelli
 * Created 5/11/17
 * In DuplicateFiles
 */

class AppGUI(@NamedArg("stage") var stage: Stage) {
    var scene: Scene
        private set


    val locationDirLabels: ObservableList<Label> = observableArrayList<Label>()
    val removeButtons: ObservableList<Button> = observableArrayList()
    val directories: ObservableList<File> = observableArrayList<File>()

    // maps directory (File) to a list of HashFiles (the files inside the directory)
    val files: ObservableMap<File, ObservableList<HashFile>> = FXCollections.observableHashMap<File, ObservableList<HashFile>>()

    val locationsPane: GridPane
    val controller: AppController = AppController(this)
    val filesView: TableView<HashFile> = TableView()
    val progressLabel: Label

    private val clearAll: Button
    private val stopButton: Button
    private val openButton: Button
    private val deleteButton: Button
    private val deleteAllButton: Button
    private val showOnDiskButton: Button
    private val addLocationButton: Button
    private val clearSelectionButton: Button
    private val exportButton: Button
    private val deleteEveryCopyButton: Button

    private val method: SimpleStringProperty = SimpleStringProperty("MD5")
    private val methods: ObservableList<String> = FXCollections.observableArrayList("MD5", "SHA1", "SHA256", "SHA512")
    private val methodLabel: Label
    internal val methodSelector = ChoiceBox<String>(methods)

    val bar = ProgressBar(0.0)

    val root: VBox = VBox()

    val recursiveBox: CheckBox

    val hashColumnText = Bindings.concat(methodSelector.selectionModel.selectedItemProperty(), " Hash")!!

    val hashColumn: TableColumn<HashFile, String> = TableColumn()

    private val gson = Gson()
    private val appPropertiesPath = "properties" + File.separator + "en_US.json"
    internal val appPreferencesPath = "properties" + File.separator + "preferences.json"
    val appProperties: AppProperties


    init {

        appProperties = gson.fromJson<AppProperties>(FileReader(appPropertiesPath), AppProperties::class.java)

        methodLabel = Label(appProperties.LabelText[AppProperties.LabelText.methodLabelText.name])
        methodLabel.prefWidth = 70.0
        methodSelector.selectionModel.select(0)
        methodSelector.prefWidth = 100.0
        method.bind(methodSelector.valueProperty())
        bar.minWidth = 250.0

        progressLabel = Label(appProperties.LabelText[AppProperties.LabelText.progressLabelText.name])
        progressLabel.minWidth = 70.0
        progressLabel.maxWidth = 70.0
        progressLabel.prefWidth = 70.0

        recursiveBox = CheckBox(appProperties.CheckBoxLabels[AppProperties.CheckBoxLabels.recursiveBoxLabel.name])
        recursiveBox.isSelected = true


        val fileNameColumn = TableColumn<HashFile, String>(appProperties.ColumnHeaderText[AppProperties.ColumnHeaderText.NameColumn.name])
        val fileColumn = TableColumn<HashFile, File>(appProperties.ColumnHeaderText[AppProperties.ColumnHeaderText.PathColumn.name])
        hashColumn.textProperty().bind(hashColumnText)

        filesView.itemsProperty().addListener { _, _, newValue ->
            if (newValue.size == 0) {
                hashColumn.textProperty().bind(hashColumnText)
            } else {
                hashColumn.textProperty().unbind()
            }
        }
        fileNameColumn.prefWidthProperty().bind(filesView.widthProperty().divide(5.0).multiply(1.45))
        fileColumn.prefWidthProperty().bind(filesView.widthProperty().divide(5.0).multiply(2.45))
        hashColumn.prefWidthProperty().bind(filesView.widthProperty().divide(5.0))

        fileNameColumn.cellValueFactory = PropertyValueFactory<HashFile, String>("name")
        fileColumn.cellValueFactory = PropertyValueFactory<HashFile, File>("file")
        hashColumn.cellValueFactory = PropertyValueFactory<HashFile, String>("hash")

        filesView.columns.addAll(fileNameColumn, fileColumn, hashColumn)
        val t = files.values.flatten()

        filesView.items = observableArrayList(t.filter { i -> t.count { it == i } > 1 })

        filesView.addEventHandler(MouseEvent.MOUSE_CLICKED) { controller.tableClickedAction(it) }


        val buttonBox = HBox()
        locationsPane = GridPane()
        addLocationButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.AddLocationButton.name])
        addLocationButton.minWidth = 130.0
        addLocationButton.prefHeight = 26.0
        addLocationButton.setOnAction { controller.addButtonAction() }

        stopButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.StopButtonText.name])
        stopButton.isDisable = true
        stopButton.minWidth = 70.0
        stopButton.prefHeight = 26.0
        stopButton.setOnAction {
            controller.hashingThread?.interrupt()
            controller.stopStatusIndicator()
        }


        AppStyle.initStyle(root)
        AppStyle.initStyle(buttonBox)
        AppStyle.initStyle(locationsPane, Insets(10.0), 10.0, 10.0)

        openButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.OpenFileButtonText.name])
        showOnDiskButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.ShowOnDiskButtonText.name])
        deleteButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.DeleteFileButtonText.name])
        deleteAllButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.DeleteAllButtonText.name])
        clearSelectionButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.ClearSelectionButtonText.name])
        clearAll = Button(appProperties.ButtonNames[AppProperties.ButtonNames.ClearAllListedButtonText.name])
        exportButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.ExportButtonText.name])
        deleteEveryCopyButton = Button(appProperties.ButtonNames[AppProperties.ButtonNames.DeleteEveryCopyText.name])

        deleteButton.setOnAction { controller.deleteAction(filesView.selectionModel.selectedItem) }
        deleteAllButton.setOnAction { controller.deleteAllDuplicatesAction() }
        openButton.setOnAction { controller.openAction(filesView.selectionModel.selectedItem) }
        showOnDiskButton.setOnAction { controller.showOnDiskAction(filesView.selectionModel.selectedItem) }
        clearSelectionButton.setOnAction { controller.clearSelectionAction(filesView) }
        clearAll.setOnAction { controller.clearAllAction() }
        exportButton.setOnAction { controller.exportToCSVAction() }
        deleteEveryCopyButton.setOnAction { controller.deleteEveryCopyAction(filesView.selectionModel.selectedItem) }

        val secondSpacer = Region()

        secondSpacer.minWidthProperty().bind(root.widthProperty()
                .divide(2)
                .subtract(bar.widthProperty())
                .subtract(progressLabel.widthProperty())
                .subtract(stopButton.widthProperty()))

        val spacer = Region()
        spacer.minWidthProperty().bind(root.widthProperty()
                .subtract(addLocationButton.widthProperty())
                .subtract(bar.widthProperty())
                .subtract(progressLabel.widthProperty())
                .subtract(stopButton.widthProperty())
                .subtract(methodSelector.widthProperty())
                .subtract(methodLabel.widthProperty())
                .subtract(secondSpacer.widthProperty())
                .subtract(20.0 * 6)) // 7 nodes, 6 "inbetweens" 20.0 pixes for each


        buttonBox.children.addAll(openButton, showOnDiskButton, deleteButton,
                deleteEveryCopyButton, deleteAllButton,
                clearSelectionButton, clearAll, exportButton)

        val headerBox = HBox(addLocationButton, spacer, methodLabel,
                methodSelector, secondSpacer, progressLabel,
                bar, stopButton)
        headerBox.alignment = Pos.CENTER_LEFT
        AppStyle.initStyle(headerBox)

        val disclaimerLabel = Label(appProperties.Messages[AppProperties.Messages.DisclaimerMessage.name])
        root.children.addAll(headerBox, recursiveBox, locationsPane, filesView,
                buttonBox, disclaimerLabel)

        val contextMenu = ContextMenu()
        val copyPathItem = MenuItem("Copy Path")
        val copyNameItem = MenuItem("Copy Name")
        val copyFilePathItem = MenuItem("Copy File and Path")
        copyNameItem.setOnAction { controller.copyNameAction(filesView.selectionModel.selectedItem) }
        copyPathItem.setOnAction { controller.copyPathAction(filesView.selectionModel.selectedItem) }
        copyFilePathItem.setOnAction { controller.copyFilePathAction(filesView.selectionModel.selectedItem) }
        contextMenu.items.addAll(copyNameItem, copyPathItem, copyFilePathItem)

        filesView.addEventHandler(MouseEvent.MOUSE_CLICKED) {
            if (it.button == MouseButton.SECONDARY) {
                filesView.selectionModel.selectedItem ?: return@addEventHandler
                val source = it.source as Node
                contextMenu.show(source, it.screenX, it.screenY)
            }
        }


        filesView.addEventHandler(KeyEvent.KEY_PRESSED) {
            it ?: return@addEventHandler
            if (it.code == KeyCode.ESCAPE) {
                filesView.selectionModel.clearSelection()
            }

            val selectedItem = filesView.selectionModel.selectedItem ?: return@addEventHandler
            if (it.isCopyEvent() || it.isCutEvent()) {
                val clipboard = Clipboard.getSystemClipboard() ?: return@addEventHandler
                val content = ClipboardContent()
                content.putFiles(listOf(selectedItem.file))
                content.putString(selectedItem.file.absolutePath)
                clipboard.setContent(content)
            } else if (it.code == KeyCode.DELETE || it.code == KeyCode.BACK_SPACE) {
                controller.deleteAction(selectedItem)
            } else if (it.code == KeyCode.ENTER && !it.isShiftDown) {
                controller.openAction(selectedItem)
            } else if (it.code == KeyCode.ENTER && it.isShiftDown) {
                controller.showOnDiskAction(selectedItem)
            }
        }

        scene = Scene(root)

    }

    internal fun updateProgressLabelText(text: String?) {
        Platform.runLater { progressLabel.text = text ?: "<not found>" }
    }

    fun disable() {
        Platform.runLater {
            addLocationButton.isDisable = true
            openButton.isDisable = true
            deleteButton.isDisable = true
            showOnDiskButton.isDisable = true
            clearSelectionButton.isDisable = true
            clearAll.isDisable = true
            deleteAllButton.isDisable = true
            methodSelector.isDisable = true
            deleteEveryCopyButton.isDisable = true
            exportButton.isDisable = true

            recursiveBox.isDisable = true
            stopButton.isDisable = false


            for (button in removeButtons) {
                button.isDisable = true
            }
        }
    }

    fun enable() {
        Platform.runLater {
            addLocationButton.isDisable = false
            openButton.isDisable = false
            deleteButton.isDisable = false
            showOnDiskButton.isDisable = false
            clearSelectionButton.isDisable = false
            clearAll.isDisable = false
            recursiveBox.isDisable = false
            deleteAllButton.isDisable = false
            methodSelector.isDisable = false
            deleteEveryCopyButton.isDisable = false
            exportButton.isDisable = false

            stopButton.isDisable = true


            for (button in removeButtons) {
                button.isDisable = false
            }
        }
    }

    fun hashMethodFunction(): (InputStream) -> String {
        return when (method.value) {
            "MD5"    -> DigestUtils::md5Hex
            "SHA1"   -> DigestUtils::sha1Hex
            "SHA256" -> DigestUtils::sha256Hex
            "SHA512" -> DigestUtils::sha512Hex
            else     -> DigestUtils::md5Hex
        }
    }


}
