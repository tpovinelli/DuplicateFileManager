package com.tom.dfm

import javafx.application.Platform
import javafx.beans.NamedArg
import javafx.collections.FXCollections
import javafx.collections.FXCollections.observableArrayList
import javafx.scene.control.*
import javafx.scene.control.Alert.AlertType
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import javafx.scene.input.MouseEvent
import javafx.stage.DirectoryChooser
import tom.utils.javafx.popupAlert
import tom.utils.thread.concurrently
import java.awt.Desktop
import java.awt.HeadlessException
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter
import java.io.InputStream
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock


/**
 * @author Thomas Povinelli
 * Created 5/11/17
 * In DuplicateFiles
 */

open class AppController(@NamedArg("gui") val gui: AppGUI) {

    @Volatile
    private var count: AtomicInteger = AtomicInteger(0)
    private val progressLock = ReentrantLock()
    private val progressCondition: Condition = progressLock.newCondition()
    var hashingThread: Thread? = null
        private set

    fun addButtonAction() {
        val chooser = DirectoryChooser()
        val file = chooser.showDialog(gui.stage) ?: return

        gui.directories.add(file)
        val theLabel = Label(file.absolutePath)
        gui.locationDirLabels.add(theLabel)

//        val button = Button("Update")
//        button.onAction = SelectButtonHandler<ActionEvent>(gui, theLabel, file)
//        gui.selectButtons.add(button)

        val removeButton = Button("Remove")
        removeButton.onAction = RemoveButtonEventHandler(gui, theLabel, file)
        gui.removeButtons.add(removeButton)

        gui.controller.updateLabelsAction()
        gui.controller.updateTableAction()

        hashingThread = concurrently(daemon = true) {
            hashDirectory(file, gui.recursiveBox.isSelected, gui.hashMethodFunction())
            stopHashingThread()
            stopStatusIndicator()
            updateTableAction()
            gui.enable()
        }
        startStatusIndicator()


    }

    private fun spawnTask(daemon: Boolean = false, block: () -> Unit): Thread {
        return concurrently(daemon = daemon, job = block)
    }

    private fun stopHashingThread() {
        hashingThread?.interrupt()
    }

    fun stopStatusIndicator() {
        gui.updateProgressLabelText(gui.appProperties.LabelText[AppProperties.LabelText.doneLabelText.name])
        count.set(0)
        gui.bar.progress = 1.0
        progressLock.withLock { progressCondition.signalAll() }

    }

    private fun startStatusIndicator() {
        gui.updateProgressLabelText(gui.appProperties.LabelText[AppProperties.LabelText.hashingLabelText.name])
        gui.disable()
        var currentCount = 0
        gui.bar.progress = 0.0
        concurrently(daemon = true) {
            while (!Thread.currentThread().isInterrupted) {
                progressLock.lock()
                try {
                    currentCount++
                    try {
                        // cannot use property access syntax due to structure of the class
                        // do not try to use the property access syntax it does not work
                        gui.bar.progress = 1.0 - ((count.get() - currentCount).toDouble() / (count.toDouble()))
                    } catch (e: ArithmeticException) {
                        e.printStackTrace()
                    }
                    progressCondition.await()
                } finally {
                    progressLock.unlock()
                }
            }
        }
    }

    private fun hashDirectory(file: File, recursively: Boolean,
                              hashMethod: (InputStream) -> String) {
        // On windows inaccessible folders appear in directory listings
        // but cannot be accessed so [File.listFiles()] returns null
        val contents: Array<out File> = file.listFiles() ?: return
        count.addAndGet(contents.size)
        gui.files[file] = observableArrayList()
        for (f in contents) {
            if (f.isDirectory) {
                if (recursively) {
                    hashDirectory(f, recursively, hashMethod)
                }
                continue
            } else {
                var fileHash = ""
                f.inputStream().use { fileHash = hashMethod(it) }
                gui.files[file]?.add(HashFile(f, fileHash))
                progressLock.withLock { progressCondition.signalAll() }
            }
        }
    }

    internal fun copyFilePathAction(selectedItem: HashFile?) {
        selectedItem ?: return
        val clipboard = Clipboard.getSystemClipboard() ?: return
        val content = ClipboardContent()
        content.putFiles(listOf(selectedItem.file))
        content.putString(selectedItem.file.absolutePath)
        clipboard.setContent(content)
    }

    internal fun copyPathAction(selectedItem: HashFile?) {
        selectedItem ?: return
        val clipboard = Clipboard.getSystemClipboard() ?: return
        val content = ClipboardContent()
        content.putString(selectedItem.file.absolutePath)
        clipboard.setContent(content)
    }

    internal fun copyNameAction(selectedItem: HashFile?) {
        selectedItem ?: return
        val clipboard = Clipboard.getSystemClipboard() ?: return
        val content = ClipboardContent()
        content.putString(selectedItem.file.name)
        clipboard.setContent(content)
    }

    fun updateLabelsAction() {

        // This should never fail
        if (Driver.debug) {
            check(gui.locationDirLabels.size == gui.removeButtons.size) {
                "Directory labels list is not the same size as " +
                        "list of selection buttons"
            }
        }

        gui.locationsPane.children.clear()

        for (i in 0 until gui.locationDirLabels.size) {
            gui.locationsPane.add(Label(gui.appProperties.LabelText[AppProperties.LabelText.locationLabelText.name]), 0,
                    i)
        }

        var index = 0
        for (label in gui.locationDirLabels) {
            gui.locationsPane.add(label, 1, index++)
        }
        index = 0
        for (button in gui.removeButtons) {
            gui.locationsPane.add(button, 3, index++)
        }
    }


    fun updateTableAction() {
        val t = gui.files.values.flatten()
        val dups = observableArrayList<HashFile>()!!
        for (item in t) {
            if (t.count { it?.hash == item?.hash } > 1) {
                dups.add(item)
            }
        }

        gui.filesView.items = dups

        FXCollections.sort(gui.filesView.items) { o1: HashFile, o2: HashFile -> o1.hash.compareTo(o2.hash) }

    }

    fun clearAllAction() {
        gui.files.clear()
        gui.removeButtons.clear()
        gui.locationDirLabels.clear()
        gui.progressLabel.text = ""
        gui.bar.progress = 0.0
        gui.filesView.items.clear()
        gui.filesView.selectionModel.clearSelection()
        gui.hashColumn.textProperty().bind(gui.hashColumnText)


        updateLabelsAction()
        updateTableAction()
    }

    private fun osOpenFile(file: File) {
        try {
            Desktop.getDesktop().open(file)
        } catch (e: Exception) {
            when (e) {
                is HeadlessException, is UnsupportedOperationException -> popupAlert("That operation is not supported on your platform. Sorry!")
                else                                                   -> throw e
            }
        }
    }

    fun openAction(selectedItem: HashFile?) {
        selectedItem ?: return
        osOpenFile(selectedItem.file)
    }

    fun showOnDiskAction(selectedItem: HashFile?) {
        selectedItem ?: return
        osOpenFile(selectedItem.file.parentFile)
    }

    fun deleteAction(selectedItem: HashFile?, withConfirm: Boolean = true) {
        selectedItem ?: return
        var result = ButtonType.CANCEL
        if (withConfirm) {
            val alert = Alert(AlertType.CONFIRMATION)
            alert.title = gui.appProperties.DeleteMessageTitle
            alert.headerText = gui.appProperties.DeleteMessageHeaderText
            alert.contentText = gui.appProperties.DeleteMessageContentText

            result = alert.showAndWait().orElse(null) ?: return
        }
        if (!withConfirm || (withConfirm && result == ButtonType.OK)) {


            // Go through all the lists containing directory contents and
            // for each one of those lists remove all the items such that
            // the item is removed if and only if it has the same hash
            // as the selected item being deleted AND it appears in the table
            // of duplicate files more than 2 times prior to deletion

            // This lets multiple duplicates stick around and removes files that
            // no longer have duplicates.

            val needDelete = ArrayList<HashFile>()

            selectedItem.file.delete()
            gui.files.values.forEach { file ->
                // identical check because if
                // frequency(gui.filesView.items, f) <= 2
                // is never true, we at least need to remove whatever we
                // deleted
                file.filterTo(needDelete) { f ->
                    (selectedItem.hash == f.hash &&
                            gui.filesView.items.count { it?.hash == f.hash } <= 2) ||
                            selectedItem === f
                }
            }

            gui.files.values.forEach { it.removeIf { needDelete.contains(it) } }

            updateTableAction()
            gui.filesView.selectionModel.clearSelection()

        } else {
            return
        }
    }

    fun tableClickedAction(event: MouseEvent?) {
        event ?: return

        if (event.clickCount == 2) {
            openAction(gui.filesView.selectionModel.selectedItem)
        }
    }


    /**
     * Runs through all the files in all the directories flattened
     * into a single collection. As it does this it places all the files
     * into lists as the values of a HashMap keyed by [Object.hashCode]
     * Since [HashFile] implements [Object.equals] and [Object.hashCode]
     * according to standards, any two [HashFile]s with the same
     * [Object.hashCode] will test as [Object.equals]
     *
     * Once this [HashMap] is made, we will go through the map and
     * remove all but one of the files in the lists containing more than one file
     *
     * Suggestions for improvement include not adding all the files to the
     * map and only adding duplicate files
     *
     * There are no guarantees or indeed mechanisms for guaranteeing the
     * order in which duplicate files are deleted. There could be
     * attempts in the future to allow selection of which files
     * are removed either individually or by some class of properties
     */
    fun deleteAllDuplicatesAction() {
        popupAlert("WARNING", "This feature has been reimplemented but has not been tested. Use caution before proceeding")
        if (askToDeleteAll()) {
            return
        }
        val allFiles = gui.files.values.flatten()
        val duplicated = mutableMapOf<Int, MutableList<HashFile>>()
        for (file in allFiles) {
            duplicated.compute(file.hashCode()) { k, v ->
                if (v != null) {
                    v.add(file)
                    return@compute v
                } else {
                    return@compute arrayListOf(file)
                }
            }
        }
        val willBeDeleted = mutableListOf<HashFile>()
        for ((_, list) in duplicated) {
            if (list.count() > 1) {
                for (idx in 1 until list.size) {
                    willBeDeleted.add(list[idx])
                }
            }
        }
        for (file in willBeDeleted) {
            file.file.delete()
            file.isDeleted = true
            gui.files.values.forEach { dirContents -> dirContents.removeIf { dirFile -> dirFile == file } }
        }
        updateTableAction()
        popupAlert("All Duplicates have been removed", "Delete All Done")
    }

    fun deleteAllAction_Old() {
        popupAlert("WARNING", "DO NOT USE THIS FEATURE. IT MAY BE FAULTY")
        if (askToDeleteAll()) {
            return
        }

        // gui.files is a map of directories -> contained files where directory (key)
        // is java.io.File and value is ObservableList<HashFile>
        val needDelete = ArrayList<HashFile>()
        val allHashedFiles = gui.files.values.toList().flatten()
        for ((i, hf) in allHashedFiles.withIndex()) {
            for ((j, otherFile) in allHashedFiles.withIndex()) {
                if (j <= i) continue
                if (hf.hash == otherFile.hash && !otherFile.isDeleted) {
                    // do not add hf, if no duplicates are found nothing is deleted
                    needDelete.add(otherFile)
                }
            }
            for (f in needDelete) {
                f.file.delete()
                f.isDeleted = true
                gui.files.values.forEach { it.removeIf { it.hash == f.hash && f.name == it.name } }
            }
            needDelete.clear()
        }
        updateTableAction()
        popupAlert("All Duplicates have been removed", "Delete All Done")
    }

    private fun askToDeleteAll(): Boolean {
        val result: ButtonType
        val alert = Alert(AlertType.CONFIRMATION)
        alert.title = "Are you sure you want to delete ALL?" //gui.appProperties.DeleteAllMessageTitle
        alert.headerText = "This will delete all duplicate files" // gui.appProperties.DeleteAllMessageHeaderText
        alert.contentText = "Are you sure you want to delete all duplicates. " +
                "This action will remove all but one copy of every file for " +
                "which there are duplicates. This process may be a little slow," +
                " especially if there are many files, you will see a confirmation" +
                " dialog when the deleting has finished. Please do not modify" +
                " the files in the meantime" // gui.appProperties.DeleteAllMessageContentText

        result = alert.showAndWait().orElse(null) ?: return true
        if (result !== ButtonType.OK) {
            if (Driver.debug)
                println("Cancelling deleteAll")
            return true
        }
        return false
    }

    /**
     * Used by the [dfm.Driver] class to load preferences and select a hash method
     */
    internal fun selectHashMethod(s: String) {
        gui.methodSelector.selectionModel.select(s)
    }

    fun clearSelectionAction(tableView: TableView<HashFile>) {
        tableView.selectionModel.clearSelection()
    }

    fun exportToCSVAction() {
        val chooser = DirectoryChooser()
        val exportLocation = chooser.showDialog(gui.stage)
        var exportFile = File(exportLocation.absolutePath + File.separator + "duplicateFiles.csv")
        var c = 1
        while (exportFile.exists()) {
            exportFile = File(exportLocation.absolutePath + File.separator + "duplicateFiles(" + c.toString() + ").csv")
            c += 1
        }

        val writer = BufferedWriter(FileWriter(exportFile))

        gui.disable()
        writer.use {
            it.write("Name,Path," + gui.hashColumnText.valueSafe + "\n")
            for (file in gui.filesView.items) {
                it.write("\"" + file.name + "\",\"" + file.file.absolutePath + "\"," + file.hash + "\n")
            }
        }
        gui.enable()
        Platform.runLater { popupAlert("Export to $exportFile complete", "Export Complete") }
    }

    fun deleteEveryCopyAction(selectedItem: HashFile?) {
        popupAlert("Not Implemented", "Not Implemented")
    }

}

