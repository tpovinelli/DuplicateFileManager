# DuplicateFileManager
A small utility meant to help manage duplicate files especially across many directories with different names

This program is written in Kotlin and Java and it is meant to facilitate in finding and remove duplicate files on a machine.

The idea is you can give it any number of folders that you have and it will recursively walk them hashing files as it goes. 
Once it has walked the whole directory and its children, it will display all the files which hashed to the same value in a table, 
which allows the user to then open, view, and delete the files. You can also add more folders that are not subdirectories of the original one.
The idea is that you can have files of the same or different names in the same or different folders that are exactly the same and taking up important free space on your machine. 
Personally, this project was inspired by my messy backup habits. In order to deal with the limited amounts of storage I have
but also maintain timely backups, I find myself fiddling with photo importing, video saving, etc. As such I have to do a lot
of manual file management which often results in me having many copies of the same things. This also occassionally happening when 
backing up code and working with version control. Currently, delete does not work, as I want to make sure it is safe before implementing it. 
Also the only way to hash files is with MD5. I realize this is not always ideal, and I will add more hash algorithms later.

This project is dependent on Kotlin and Java 8 

This project is also dependent on [TomUtils](https://github.com/elunico/TomUtils) which is a library I wrote in Kotlin
for doing various things that are tedious or otherwise verbose. 


<code>(c) 2017. Thomas Povinelli. All Rights Reserved.</code>
